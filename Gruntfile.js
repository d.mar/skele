// -----------------------------------------------------------------------------
// Gruntjs config.
// -----------------------------------------------------------------------------
// @autor        David Martín <hola@dmar.dev>
// @copyright    public domain - 2023
// -----------------------------------------------------------------------------

'use strict';

const sass = require('sass');
const mozjpeg = require('imagemin-mozjpeg');

// ----------------------------------------------------------------------------
// Defined paths
// ----------------------------------------------------------------------------

var paths = {
    default: './',
    src: {
        default: 'src/assets/',
        scss: 'src/assets/scss/',
        js: 'src/assets/js/',
        fonts: 'src/assets/fonts/',
        img: 'src/assets/images/',
    },
    dist: {
        default: 'public/assets/',
        css: 'public/assets/css',
        js: 'public/assets/js',
        fonts: 'public/assets/fonts',
        img: 'public/assets/img'
    },
}

// ----------------------------------------------------------------------------
// File formats & exts.
// ----------------------------------------------------------------------------

var imageFileFormats = '**/*.{png,jpg,jpeg,gif}';
var sassFileFormats = '**/*.{scss,sass}';
var jsFileFormats = '**/*.js';
var fontFileFormats = '*.{ttf,woff,woff2,svg,eot}';

// ----------------------------------------------------------------------------
// Mains.
// ----------------------------------------------------------------------------

var mainCss = 'main.min.css';
var mainJavaScript = 'main.min.js';

// ----------------------------------------------------------------------------
// Tasks.
// ----------------------------------------------------------------------------

module.exports = function(grunt){
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        watch: {
            sass: {
                files: [paths.src.scss + sassFileFormats],
                tasks: ['sass', 'cssmin']
            },
            js: {
                files: [paths.src.js + jsFileFormats],
                tasks: ['uglify']
            },
            img: {
                files: [paths.src.img + imageFileFormats],
                tasks: ['imagemin']
            },
            fonts: {
                files: [paths.src.fonts + fontFileFormats],
                tasks: ['copy']
            },
         },
        imagemin: {
            static: {
                options: {
                    optimizationLevel: 3,
                    svgoPlugins: [{removeViewBox: false}],
                    use: [mozjpeg()]
                },
                files: [{
                    expand: true,
                    cwd: paths.src.img,
                    src: imageFileFormats,
                    dest: paths.dist.img,
                }]
            },
            // dynamic: {
            //     files: [{
            //         expand: true,
            //         cwd: paths.src.img,
            //         src: imageFileFormats,
            //         dest: paths.dist.img,
            //     }]
            // }
        },
        sass: {
            dist: {
                options: {
                    implementation: sass,
                    compass: false,
                    quiet: true,
                    style: 'expanded',
                    sourcemap: 'auto',
                },
                files: [{
                    expand: true,
                    cwd: paths.src.scss,
                    src: sassFileFormats,
                    dest: paths.dist.css,
                    ext: '.css',
                }]
            }
        },
        cssmin: {
            target: {
                files: [{
                    expand: true,
                    cwd: paths.dist.css,
                    src: ['*.css', '!*.min.css'],
                    dest: paths.dist.css,
                    ext: '.min.css'
                }]
            }
        },
        uglify: {
            options: {
                compress: {
                    drop_console: true
                }
            },
            all:{
                files: {
                    'public/assets/js/main.min.js': [

                        // Main app file.
                        paths.src.js + mainJavaScript,
                    ]
                }
                // files: [{
                //     expand: true,
                //     cwd: paths.src.js,
                //     src: jsFileFormats,
                //     dest: paths.dist.js,
                //     ext: '.min.js'
                // }]
            }
        },
        copy: {
            main: {
                files: [{
                    expand: true,
                    cwd: paths.src.fonts,
                    src: fontFileFormats,
                    dest: paths.dist.fonts
                }],
            }
        },
    });
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.registerTask(
        'default',
        [
            'watch',
            'sass',
            'uglify',
        ]
    );
};
