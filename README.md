# The-Skele

Just a small skele for personal projects.

- Sublime Text support. [sublimetext.com](https://www.sublimetext.com/)
- SASS support. [sass-lang.com/](https://sass-lang.com/)
- Editorconfig support. [editorconfig.org](https://editorconfig.org/)
- Sermver support. [semver.org](https://semver.org/)
- Composer support. [getcomposer.org](https://getcomposer.org)
- Grunt.js support. [gruntjs.com](https://gruntjs.com/)
- Gulp support. [gulpjs.com](https://gulpjs.com)
- PHPUnit support. [phpunit.de](https://phpunit.de/)
- Gitlab CI support. [gitlab.com](https://docs.gitlab.com/ee/topics/build_your_application.html)
- Semantic-release support. [https://github.com/semantic-release/semantic-release](https://github.com/semantic-release/semantic-release)

<!-- GETTING STARTED -->
## Getting Started

Use [Composer](https://getcomposer.org) to create the project, choose the desired JavaScript toolkit/task-runner to use (currently Gruntjs/Gulp) and launch the runner. Assets inside the src folder will run the next steps to dist/public destination:

* Sass files - Sass compiler, minify and move to dist/public folder.
* JavaScript files - Uglify, compress and move to dist/public folder.
* Images - Compress and move to dist/public.
* Fonts - Copy to dist/public folder.

Gitlab CI/CD has been set to use 4 Stages: build, test, deploy and release. Jobs under test stage cover code quality and a simple code coverage. Release
job would run the semantic-release support under Gitlab.

Create project:

```sh
composer create-project dmardev/skele
```

Post install script should ask for user input to install the desired task-runner (gruntjs/gulp), with the watch task ready.

### Gitlab CI/CD
Check Gitlab CI/CD to enable pipelines.

### Manually install Tasks runners
Install the desired JS toolkit/task-runner manually by composer post-install-cmd shell script:

```sh
bash src/scripts/composer-post-create-project.sh
```

For gruntjs, use/edit the helper shell script:

```sh
bash src/scripts/install-gruntjs.sh
```
or:

1.  Install node.js & npm:

    ```sh
    sudo apt-get install nodejs npm
    node -v && npm -v
    ```

2.  Install Gulp globally:

    ```sh
    sudo npm install -g grunt-cli
    ```

3. Install Grunt related packages:

    ```sh
    npm install --save-dev grunt grunt-contrib-imagemin grunt-contrib-copy imagemin-mozjpeg@7.0.0 grunt-contrib-cssmin grunt-contrib-sass grunt-contrib-uglify grunt-contrib-watch grunt-sass sass
    ```

For Gulp, use/edit the helper shell script:

```sh
bash src/scripts/install-gulp.sh
```
or:

1.  Install node.js & npm:

    ```sh
    sudo apt-get install nodejs npm
    node -v && npm -v
    ```

2.  Install Gulp globally:

    ```sh
    sudo npm install -g gulp-cli
    ```

3.  Install Gulp related packages:

    ```sh
    npm install --save-dev gulp gulp-clean-css gulp-imagemin@7.0.0 gulp-concat gulp-cssnano gulp-file gulp-sass gulp-sourcemaps gulp-uglify gulp-uglifycss gulp-watch sass
    ```

Run the task runner:

Gulp:
```sh
gulp watch &
```

Gruntjs:
```sh
grunt watch &
```

To stop the task runner:

Gulp:
```sh
killall gulp
```

Gruntjs:
```sh
killall grunt
```

<!-- TODO -->
## TODO

- Add webpack support.

<!-- LICENSE -->
## License

Distributed under the Public domain license. See `LICENSE` for more information.

<!-- CONTACT -->
## Contact

David Martín - hola@dmar.dev - [https://dmar.dev](https://dmar.dev/) 

Project Link: [https://gitlab.com/d.mar/skele](https://gitlab.com/d.mar/skele)
