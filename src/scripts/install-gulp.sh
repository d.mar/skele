#!/bin/bash
# -----------------------------------------------------------------------------
# Gulp installer.
# -----------------------------------------------------------------------------
#
# @autor        David Martín <hola@dmar.dev>
# @copyright    public domain - 2023
# -----------------------------------------------------------------------------

PACKAGES="gulp gulp-clean-css gulp-imagemin@7.0.0 gulp-concat gulp-cssnano gulp-file gulp-sass gulp-sourcemaps gulp-uglify gulp-uglifycss gulp-watch sass";

# First, lets remove all dependecies, just in case we change the task-runner.
if [[ -f "package.json" ]] && [[ -d "node_modules" ]]; then
    printf "\nFirst, remove all dependecies.\n";
    npm uninstall `ls -1 node_modules | tr '/\n' ' '`
fi

# Let's install Debian packages for NPM.
if ! [[ -x "$(command -v nodejs --version)" ]]; then
    sudo apt-get install nodejs
fi
if ! [[ -x "$(command -v npm --version)" ]]; then
    sudo apt-get install npm
fi
if ! [[ -x "$(command -v grunt --version)" ]]; then
    sudo npm install -g gulp-cli
fi

# Let's install Grunt packages.
if [[ -x "$(command -v npm --version)" ]]; then
    npm install $PACKAGES --save-dev && npm list
fi
exit 0
