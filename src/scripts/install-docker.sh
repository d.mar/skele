#!/bin/bash
# -----------------------------------------------------------------------------
# Docker installer
# -----------------------------------------------------------------------------
#
# @autor        David Martín <hola@dmar.dev>
# @copyright    public domain - 2023
# -----------------------------------------------------------------------------
# https://docs.docker.com/engine/install/debian/#install-using-the-repository

# Setup Docker repo
sudo apt-get install ca-certificates curl gnupg && sudo install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
sudo chmod a+r /etc/apt/keyrings/docker.gpg
echo \
  "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
  "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update

# Install latest docker packages.
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

# Check it!
docker --version
docker compose version
