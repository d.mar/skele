#!/bin/bash
# -----------------------------------------------------------------------------
# Gruntjs installer.
# -----------------------------------------------------------------------------
#
# @autor        David Martín <hola@dmar.dev>
# @copyright    public domain - 2023
# -----------------------------------------------------------------------------

PACKAGES="grunt grunt-contrib-imagemin grunt-contrib-copy imagemin-mozjpeg@7.0.0 grunt-contrib-cssmin grunt-contrib-sass grunt-contrib-uglify grunt-contrib-watch grunt-sass sass";

# First, lets remove all dependecies, just in case we change the task-runner.
if [[ -f "package.json" ]] && [[ -d "node_modules" ]]; then
    printf "\nFirst, remove all dependecies.\n";
    npm uninstall `ls -1 node_modules | tr '/\n' ' '`
fi

# Let's install Debian packages for NPM.
if ! [[ -x "$(command -v nodejs --version)" ]]; then
    sudo apt-get install nodejs
fi
if ! [[ -x "$(command -v npm --version)" ]]; then
    sudo apt-get install npm
fi
if ! [[ -x "$(command -v grunt --version)" ]]; then
    sudo npm install -g grunt-cli
fi

# Let's install Grunt packages.
if [[ -x "$(command -v npm --version)" ]]; then
    npm install $PACKAGES --save-dev && npm list
fi
exit 0
