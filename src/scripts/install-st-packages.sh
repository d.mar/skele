#!/bin/bash
# -----------------------------------------------------------------------------
# ST4 package installer.
# -----------------------------------------------------------------------------
#
# @autor        David Martín <hola@dmar.dev>
# @copyright    public domain - 2023
# -----------------------------------------------------------------------------


ST_PACKAGES="EditorConfig,StringUtilities,Sass,Indent XML,JsonTree,Minify,Pretty JSON";

GREEN="\033[0;32m";
NC="\033[0m";

# -----------------------------------------------------------------------------
# 1. Checking for ST and installing it if needed.
# -----------------------------------------------------------------------------

if [[ ! -x "$(command -v subl --help)" ]]; then
    printf "\nInstalling Sublime Text 4 from sublimetext.com main repositories.\n";
    if [[ ! -x "$(command -v curl --version)" ]]; then
        sudo apt-get install curl
    fi
    curl -fsSL https://download.sublimetext.com/sublimehq-pub.gpg | sudo gpg --dearmor -o /etc/apt/trusted.gpg.d/sublime.gpg
    sudo apt-get install apt-transport-https && echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.listecho "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list
    sudo apt-get update && sudo apt-get install sublime-text
    printf "\n${GREEN}Done.${NC}\n";
fi


# -----------------------------------------------------------------------------
# 2. Adding ST packages
# -----------------------------------------------------------------------------

if [[ -x "$(command -v subl --version)" ]]; then
    printf "\nInstalling Package Control and "${ST_PACKAGES}" packages.\n";
    subl --command "install_package_control"
    subl --command "advanced_install_package {\"packages\": \"${ST_PACKAGES}\"}"
    printf "\n${GREEN}Done.${NC}\n";
    exit 0
else
    printf "\nThere was a problem looking for subl cli command (Maybe not in PATH yet). Aborting.\n";
    exit 1
fi
