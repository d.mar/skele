#!/bin/bash
# -----------------------------------------------------------------------------
# Composer Post install.
# -----------------------------------------------------------------------------
#
# @autor        David Martín <hola@dmar.dev>
# @copyright    public domain - 2023
# -----------------------------------------------------------------------------


# Chose task runner.
# -----------------------------------------------------------------------------

read -p "Want me to install a task-runner? (y/n) " STEP_INSTALL
if ! [[ "$STEP_INSTALL" = "y" ]]; then
    printf "\nOk, exiting ..\n";
    exit 0
fi
while [[ -z "$STEP_CHOOSE" ]] && [[ "$STEP_CHOOSE" != "grunt" ]] && [[ "$STEP_CHOOSE" != "gulp" ]]; do
    read -p "What one should install? (grunt/gulp) " STEP_CHOOSE
    if [[ "$STEP_CHOOSE" = "grunt" ]]; then
        printf "\nInstalling Grunjs task runner.\n";
        bash "src/scripts/"install-gruntjs.sh
    elif [[ "$STEP_CHOOSE" = "gulp" ]]; then
        printf "\nInstalling Gulp task runner.\n";
        bash "src/scripts/"install-gulp.sh
    fi
done
