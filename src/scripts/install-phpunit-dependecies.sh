#!/bin/bash
# -----------------------------------------------------------------------------
# Install PHPUnit's php-cli dependencies
# -----------------------------------------------------------------------------
#
# @autor        David Martín <hola@dmar.dev>
# @copyright    public domain - 2023
# -----------------------------------------------------------------------------

PHPCLI_INI_PATH=php -i | grep "Configuration File (php.ini) Path" | sed 's/[^/]*//';
PHPCLI_INI_FILE=$PHPCLI_INI_PATH"/php.ini";
TS=$(date +%Y_%m_%d_%H%M%S);

GREEN="\033[0;32m";
NC="\033[0m";

printf "\nHelper to install PHPUnit's php-cli packages (Debian).";
printf "\n${GREEN}David Martín - <hola@dmar.dev> | public domain - 2023${NC}\n";

# -----------------------------------------------------------------------------
# 1. Install packages.
# -----------------------------------------------------------------------------

sudo apt-get install php-cli php-json php-mbstring php-xml php-pcov php-xdebug

# -----------------------------------------------------------------------------
# 2. Patch php-cli php.ini file.
# -----------------------------------------------------------------------------

printf "\nPHPUnit recommends:\n";
printf "\nerror_reporting=-1\n";
printf "\nlog_errors_max_len=0\n";
printf "\nxdebug.show_exception_trace=0\n";
printf "\nxdebug.mode=[..],coverage\n";
printf "\nzend.assertions=1\n";
printf "\nassert.exception=1\n";
printf "\nmemory_limit=-1 <= It's default.\n";
php -i | grep 'Configuration File';

read -p "\nWe should patch PHP-cli php.ini with PHPUnit desired configurationi directives. ¿continue? (y/n)" step
if [ "$step" = "n" ]; then
  printf "\nOk, exiting ... \n";
  exit 0
fi

sudo cp PHPCLI_INI_FILE PHPCLI_INI_PATH"/php.ini.bak-"$TS;
sed -i '/error_reporting/c\error_reporting=-1\' PHPCLI_INI_FILE;
sed -i '/log_errors_max_len/c\log_errors_max_len=0\' PHPCLI_INI_FILE;
sed -i '/debug.show_exception_trace/c\xdebug.show_exception_trace=0\' PHPCLI_INI_FILE
sed -i '/xdebug.mode/s/$/,coverage/' PHPCLI_INI_FILE
sed -i '/zend.assertions/c\zend.assertions=1\' PHPCLI_INI_FILE
sed -i '/assert.exception/c\assert.exception=1\' PHPCLI_INI_FILE

printf "\nOk, patch done.\n";

# -----------------------------------------------------------------------------
# 2. Installing PHPUnit globally,
# -----------------------------------------------------------------------------

read -p "\nDo you want to install PHPUnit globally? ¿continue? (y/n)" step
if [ "$step" = "n" ]; then
  printf "\nOk, exiting ... \n";
  exit 0
fi

# https://docs.phpunit.de/en/10.3/installation.html#configuring-php-for-development
wget -O phpunit.phar https://phar.phpunit.de/phpunit-10.phar
chmod +x phpunit.phar
sudo mv phpunit.phar /usr/local/bin/phpunit
phpunit --version

printf "\nOk, done.\n";
exit 0
