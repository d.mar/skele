// -----------------------------------------------------------------------------
// Gulp config.
// -----------------------------------------------------------------------------
// @autor        David Martín <hola@dmar.dev>
// @copyright    public domain - 2023
// -----------------------------------------------------------------------------

'use strict';

const sass = require('gulp-sass')(require('sass'));
const gulp = require('gulp');
const concat = require('gulp-concat');
const cssnano = require('gulp-cssnano');
const uglify = require('gulp-uglify');
const watch = require('gulp-watch');
const imagemin = require('gulp-imagemin');


// ----------------------------------------------------------------------------
// Defined paths
// ----------------------------------------------------------------------------

var paths = {
    default: './',
    src: {
        default: 'src/assets/',
        scss: 'src/assets/scss/',
        js: 'src/assets/js/',
        fonts: 'src/assets/fonts/*',
        img: 'src/assets/images/',
    },
    dist: {
        default: 'public/assets/',
        css: 'public/assets/css',
        js: 'public/assets/js',
        fonts: 'public/assets/fonts',
        img: 'public/assets/img'
    },
}

// ----------------------------------------------------------------------------
// File formats & exts.
// ----------------------------------------------------------------------------

var imageFileFormats = '**/*.{png,jpg,jpeg,gif}';
var sassFileFormats = '**/*.{scss,sass}';
var jsFileFormats = '**/*.js';
var fontFileFormats = '**/*.{ttf,woff,woff2,svg,eot}';

// ----------------------------------------------------------------------------
// Mains.
// ----------------------------------------------------------------------------

var mainCss = 'main.min.css';
var mainJavaScript = 'main.min.js';

// ----------------------------------------------------------------------------
// Tasks.
// ----------------------------------------------------------------------------

gulp.task('sassTask', sassTask);
function sassTask() {
    return gulp.src(paths.src.scss + sassFileFormats)
        .pipe(sass({outputStyle: 'compressed' }).on('error', sass.logError))
        .pipe(concat(mainCss))
        .pipe(cssnano())
        .pipe(gulp.dest(paths.dist.css));
};
gulp.task('jsTask', jsTask);
function jsTask() {
    return gulp.src(paths.src.js + jsFileFormats)
        .pipe(concat(mainJavaScript))
        .pipe(uglify())
        .pipe(gulp.dest(paths.dist.js));
}
gulp.task('imageminTask', imageminTask);
function imageminTask() {
    return gulp.src(paths.src.img + imageFileFormats)
        .pipe(imagemin([
            imagemin.gifsicle({interlaced: true}),
            imagemin.mozjpeg({quality: 75, progressive: true}),
            imagemin.optipng({optimizationLevel: 5}),
            imagemin.svgo({
                plugins: [
                    {removeViewBox: true},
                    {cleanupIDs: false}
                ]
            })
        ]))
        .pipe(gulp.dest(paths.dist.img));
}
gulp.task('fontsTask', fontsTask);
function fontsTask() {
    return gulp.src(paths.src.fonts + fontFileFormats)
        .pipe(gulp.dest(paths.dist.fonts));

}
exports.scss = gulp.parallel(sassTask);
exports.js = gulp.parallel(jsTask);
exports.imagemin = gulp.parallel(imageminTask);
exports.fonts = gulp.parallel(fontsTask);

// ----------------------------------------------------------------------------
// Watch task
// ----------------------------------------------------------------------------

gulp.task('watch', function(){
    gulp.watch(paths.src.scss, exports.scss);
    gulp.watch(paths.src.js, exports.js);
    gulp.watch(paths.src.img, exports.imagemin);
    gulp.watch(paths.src.fonts);
});
